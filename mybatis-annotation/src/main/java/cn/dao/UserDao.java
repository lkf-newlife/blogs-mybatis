package cn.dao;

import cn.pojo.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserDao {
    @Select("select * from t_user")
    List<User> findAll();
}
