package cn.pojo;

import lombok.ToString;

import java.util.Date;

@ToString
public class User {
    private Integer id;
    private String username;
    private Date birthday;
    private String six;
    private String address;
}
