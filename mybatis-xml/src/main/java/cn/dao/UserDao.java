package cn.dao;

import cn.pojo.User;

import java.util.List;

public interface UserDao {
    List<User> findAll();
    /**
     * 注：在实际发开中，都是越简单越好，故不采用写Dao实现类的方式，使用xml或annotation都行。
     * 虽然不推荐写Dao实现类，但是mybatis是支持写实现类的。
     */
}
