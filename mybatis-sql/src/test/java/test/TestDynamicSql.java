package test;

import cn.mapper.UserMapper;
import cn.pojo.User;
import cn.pojo.UserDto;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * 动态SQL测试类
 */
public class TestDynamicSql {

    private InputStream inputStream = null;
    private SqlSession session = null;
    private UserMapper userMapper = null;

    @Before
    public void init() throws IOException {
        // 1.读取配置文件
        inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        // 2.创建SqlSessionFactory工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        // 3.使用工厂生产SqlSession对象
        session = factory.openSession();
        // 4.使用SqlSession创建Dao接口的代理对象
        userMapper = session.getMapper(UserMapper.class);
        // 5.执行对象方法,即下面的各个测例
    }

    @After
    public void destroy() throws IOException {
        // 6.释放资源
        session.close();
        inputStream.close();
    }

    /**
     * if标签测试
     * <p>
     * 需求：如果名字不为空，则根据名字查找用户
     * 如果地址也不为空，则根据名字和地址查找用户
     * 如果两者都为空，则查找全部用户
     * <p>
     * 用于多条件查询，比如书城买书时根据书名、作者等信息进行综合查找
     * <p>
     * 查找名字中含有"王"字，且住址是"北京"的人(可以注释条件观察控制台打印的sql语句)
     */
    @Test
    public void test1() {
        User user = new User();
        user.setUserName("王");
        user.setUserAddress("北京");
        List<User> users = userMapper.findUserByUsernameAndAddress(user);
        users.forEach(u -> System.out.println(u));
        /**
         * 结果
         * (1) 条件只有 user.setUserName("王");
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         * User(userId=47, userName=王六, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=洛阳)
         *
         * (2) 条件只有 user.setUserAddress("北京");
         * User(userId=41, userName=张三, userBirthday=2018-02-27 17:47:08, userSex=男, userAddress=北京)
         * User(userId=42, userName=小明, userBirthday=2018-03-02 15:09:37, userSex=女, userAddress=北京金燕龙)
         * User(userId=43, userName=小明, userBirthday=2018-03-04 11:34:34, userSex=女, userAddress=北京金燕龙)
         * User(userId=45, userName=李四, userBirthday=2018-03-04 12:04:06, userSex=男, userAddress=北京金燕龙)
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         * User(userId=48, userName=赵六, userBirthday=2018-03-08 11:44:00, userSex=女, userAddress=北京修正)
         *
         * (3) 条件有 user.setUserName("王"); 和 user.setUserAddress("北京");
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         *
         * (4) 无条件(即上面两个set赋值均注释掉)
         * 结果为所有用户、同上面 testEnv 测试结果,因此此处略
         */
    }

    /**
     * choose、when、otherwise标签测试
     * <p>
     * 需求：当用户名不为空时，则只根据用户名进行筛选 (即优先级用户名大于地址)
     * 当用户名为空，而住址不为空时，则只根据住址进行筛选
     * 当用户名和住址都为空时，则筛选所有男性用户
     */
    @Test
    public void test2() {
        User user = new User();
//        user.setUserName("王");
//        user.setUserAddress("北京");
        List<User> users = userMapper.findUserByUsernameOrAddress(user);
        users.forEach(u -> System.out.println(u));
        /**
         * 结果
         * (1) 条件只有 user.setUserName("王");
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         * User(userId=47, userName=王六, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=洛阳)
         *
         * (2) 条件只有 user.setUserAddress("北京");
         * User(userId=41, userName=张三, userBirthday=2018-02-27 17:47:08, userSex=男, userAddress=北京)
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         *
         * (3) 条件有 user.setUserName("王"); 和 user.setUserAddress("北京");
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         * User(userId=47, userName=王六, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=洛阳)
         *
         * (4) 无条件(即上面两个set赋值均注释掉)
         * User(userId=41, userName=张三, userBirthday=2018-02-27 17:47:08, userSex=男, userAddress=北京)
         * User(userId=45, userName=李四, userBirthday=2018-03-04 12:04:06, userSex=男, userAddress=北京金燕龙)
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         * User(userId=47, userName=王六, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=洛阳)
         */
    }


    /**
     * where标签测试
     * <p>
     * 用where标签替换 "select * from user where 1=1 "中的 " 1=1 " ,
     * 在前面的sql中若是没有 "1=1"，导致where后直接拼接and，造成sql语法错误
     */
    @Test
    public void test3() {
        User user = new User();
        user.setUserName("王");
        List<User> users = userMapper.findUserByWhere(user);
        users.forEach(u -> System.out.println(u));
        /**
         * 无条件结果
         * 即查询所有用户，结果同testEnv，略
         *
         * 含条件 user.setUserName("王");
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         * User(userId=47, userName=王六, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=洛阳)
         */
    }


    /**
     * trim标签测试
     * <p>
     * 除了使用where外，还可以使用trim来制定需要的功能。
     * trim的功能是去除一些特殊的字符串。
     */
    @Test
    public void test4() {
        User user = new User();
        user.setUserName("王");
        List<User> users = userMapper.findUserByTrim(user);
        users.forEach(u -> System.out.println(u));
        /**
         * 无条件结果
         * 即查询所有用户，结果同testEnv，略
         *
         * 含条件 user.setUserName("王");
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         * User(userId=47, userName=王六, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=洛阳)
         */
    }


    /**
     * set标签测试
     * <p>
     * 在更新的时候，大多数情况下都是更新的某一个或几个字段，
     * 若是将一条数据的所有属性都更新一遍，那么执行效率非常差，
     * 因此可以用mybatis中的set关键字解决此问题。
     */
    @Test
    public void test5() {
        User user = new User();
        user.setUserId(43);
        user.setUserName("刘小邦");
        user.setUserAddress("咸阳");
        userMapper.updateUserBySet(user);
    }

    /**
     * foreach标签测试
     * <p>
     * mybatis中提供了一种用于数组和集合循环遍历的方式。
     * foreach常和in连用。
     */
    @Test
    public void test6() {
        List<Integer> ids = Arrays.asList(41, 43, 46, 48);
        List<User> users = userMapper.findUsersByIds(ids);
        users.forEach(u -> System.out.println(u));
        /**
         * User(userId=41, userName=张三, userBirthday=2018-02-27 17:47:08, userSex=男, userAddress=北京)
         * User(userId=43, userName=刘小邦, userBirthday=2018-03-04 11:34:34, userSex=女, userAddress=咸阳)
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         * User(userId=48, userName=赵六, userBirthday=2018-03-08 11:44:00, userSex=女, userAddress=北京修正)
         */
    }

    /**
     * bind 标签测试
     * <p>
     * 在进行sql模糊查询的时候，如果使用"${}"，则无法防止sql注入问题；
     * 如果使用concat()函数进行连接，则只针对mysql数据库有效；
     * 若是使用oracle数据库，则需要使用"||"
     * 因此，mybatis提供了bind标签来解决这个问题
     */
    @Test
    public void test7() {
        User user = new User();
        user.setUserName("六");
        List<User> users = userMapper.findUserByBind(user);
        users.forEach(u -> System.out.println(u));
        /**
         * User(userId=47, userName=王六, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=洛阳)
         * User(userId=48, userName=赵六, userBirthday=2018-03-08 11:44:00, userSex=女, userAddress=北京修正)
         */
    }


    /**
     * sql标签测试
     * <p>
     * 可将重复的sql提取出来，使用时include标签引入，
     * 这样不仅可以使代码简洁，而且还能达到sql重用的目的。
     * <p>
     * 特别是一张表中字段非常多时，假设50个，好多查询的sql语句都只需要其中20个字段，
     * 那么此时sql标签就非常有用，把这经常用到的20个字段用sql标签定义用的时候引入即可
     */
    @Test
    public void test8() {
        List<UserDto> users = userMapper.findUserBySql();
        users.forEach(u -> System.out.println(u));

        /**
         * UserDto(userName=张三, userSex=男, userAddress=北京)
         * UserDto(userName=小明, userSex=女, userAddress=北京金燕龙)
         * UserDto(userName=刘小邦, userSex=女, userAddress=咸阳)
         * UserDto(userName=李四, userSex=男, userAddress=北京金燕龙)
         * UserDto(userName=王五, userSex=男, userAddress=北京)
         * UserDto(userName=王六, userSex=男, userAddress=洛阳)
         * UserDto(userName=赵六, userSex=女, userAddress=北京修正)
         */
    }
}
