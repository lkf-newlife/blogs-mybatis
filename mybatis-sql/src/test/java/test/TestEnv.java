package test;

import cn.mapper.UserMapper;
import cn.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestEnv {

    private InputStream inputStream = null;
    private SqlSession session = null;
    private UserMapper userMapper = null;

    @Before
    public void init() throws IOException {
        // 1.读取配置文件
        inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        // 2.创建SqlSessionFactory工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        // 3.使用工厂生产SqlSession对象
        session = factory.openSession();
        // 4.使用SqlSession创建Dao接口的代理对象
        userMapper = session.getMapper(UserMapper.class);
        // 5.执行对象方法,即下面的各个测例
    }

    @After
    public void destroy() throws IOException {
        // 6.释放资源
        session.close();
        inputStream.close();
    }

    /**
     * findAll环境测试
     */
    @Test
    public void testEnv() {
        List<User> list = userMapper.findAll();
        for (User user : list) {
            System.out.println(user);
        }
        /**
         * 结果
         * User(userId=41, userName=张三, userBirthday=2018-02-27 17:47:08, userSex=男, userAddress=北京)
         * User(userId=42, userName=小明, userBirthday=2018-03-02 15:09:37, userSex=女, userAddress=北京金燕龙)
         * User(userId=43, userName=小明, userBirthday=2018-03-04 11:34:34, userSex=女, userAddress=北京金燕龙)
         * User(userId=45, userName=李四, userBirthday=2018-03-04 12:04:06, userSex=男, userAddress=北京金燕龙)
         * User(userId=46, userName=王五, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=北京)
         * User(userId=47, userName=王六, userBirthday=2018-03-07 17:37:26, userSex=男, userAddress=洛阳)
         * User(userId=48, userName=赵六, userBirthday=2018-03-08 11:44:00, userSex=女, userAddress=北京修正)
         */
    }
}
