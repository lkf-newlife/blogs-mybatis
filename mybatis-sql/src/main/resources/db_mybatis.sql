-- scp db.sql root@192.168.181.160:/tmp
-- 进入mysql客户端执行命令"source /tmp/db.sql;"
create database if not exists db_mybatis character set utf8;
use db_mybatis;

DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
`id` int(11) NOT NULL auto_increment,
`username` varchar(32) NOT NULL COMMENT '用户名称',
`birthday` datetime default NULL COMMENT '生日',
`sex` char(1) default NULL COMMENT '性别',
`address` varchar(256) default NULL COMMENT '地址',
PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `t_user`(`id`,`username`,`birthday`,`sex`,`address`)
values  (41,'张三','2018-02-27 17:47:08','男','北京'),
(42,'小明','2018-03-02 15:09:37','女','北京金燕龙'),
(43,'小明','2018-03-04 11:34:34','女','北京金燕龙'),
(45,'李四','2018-03-04 12:04:06','男','北京金燕龙'),
(46,'王五','2018-03-07 17:37:26','男','北京'),
(47,'王六','2018-03-07 17:37:26','男','洛阳'),
(48,'赵六','2018-03-08 11:44:00','女','北京修正');