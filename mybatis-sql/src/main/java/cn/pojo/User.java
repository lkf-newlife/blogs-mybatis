package cn.pojo;

import lombok.Data;

@Data
public class User {
    /**
     * 加个 user 前缀,特意与数据库中的列不一样,测试 ResultMap
     * <p>
     * 不同的数据类型 ResultMap 映射结果如下
     * private Date userBirthday;    ==>  userBirthday=Tue Feb 27 17:47:08 CST 2018
     * private String userBirthday;  ==>  userBirthday=2018-02-27 17:47:08
     */
    private Integer userId;
    private String userName;
    // private Date userBirthday;
    private String userBirthday;
    private String userSex;
    private String userAddress;
}
