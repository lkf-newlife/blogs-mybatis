package cn.pojo;

import lombok.Data;

@Data
public class UserDto {
    /**
     * User的部分属性
     * 假如 t_user 表中有50个字段，那么User类就有与之对应的50个属性
     * 而大部分查询时只需要查其中的20个属性，因此可以把该20个属性单独定义成一个dto对象
     */
    private String userName;
    private String userSex;
    private String userAddress;
}
