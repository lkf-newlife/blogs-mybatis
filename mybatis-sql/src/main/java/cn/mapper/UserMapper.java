package cn.mapper;

import cn.pojo.User;
import cn.pojo.UserDto;

import java.util.List;

public interface UserMapper {
    // 查询所有用户,主要用于环境测试
    List<User> findAll();

    /**
     * 需求：如果名字不为空，则根据名字查找用户
     * 如果地址也不为空，则根据名字和地址查找用户
     * 如果两者都为空，则查找全部用户
     * <p>
     * 用于多条件查询，比如书城买书时根据书名、作者等信息进行综合查找
     */
    List<User> findUserByUsernameAndAddress(User user);

    /**
     * 需求：当用户名不为空时，则只根据用户名进行筛选 (即优先级用户名大于地址)
     * 当用户名为空，而住址不为空时，则只根据住址进行筛选
     * 当用户名和住址都为空时，则筛选所有男性用户
     */
    List<User> findUserByUsernameOrAddress(User user);

    /**
     * 用where标签替换 "select * from user where 1=1 "中的 " 1=1 " ,
     * 在前面的sql中若是没有 "1=1"，导致where后直接拼接and，造成sql语法错误
     */
    List<User> findUserByWhere(User user);

    /**
     * 除了使用where外，还可以使用trim来制定需要的功能。
     * trim的功能是去除一些特殊的字符串。
     */
    List<User> findUserByTrim(User user);

    /**
     * 在更新的时候，大多数情况下都是更新的某一个或几个字段，
     * 若是将一条数据的所有属性都更新一遍，那么执行效率非常差，
     * 因此可以用mybatis中的set关键字解决此问题。
     */
    List<User> updateUserBySet(User user);

    /**
     * mybatis中提供了一种用于数组和集合循环遍历的方式。
     * foreach常和in连用。
     */
    List<User> findUsersByIds(List<Integer> ids);


    /**
     * 在进行sql模糊查询的时候，如果使用"${}"，则无法防止sql注入问题；
     * 如果使用concat()函数进行连接，则只针对mysql数据库有效；
     * 若是使用oracle数据库，则需要使用"||"
     * 因此，mybatis提供了bind标签来解决这个问题
     */
    List<User> findUserByBind(User user);

    /**
     * 可将重复的sql提取出来，使用时include标签引入，
     * 这样不仅可以使代码简洁，而且还能达到sql重用的目的。
     * <p>
     * 特别是一张表中字段非常多时，假设50个，好多查询的sql语句都只需要其中20个字段，
     * 那么此时sql标签就非常有用，把这经常用到的20个字段用sql标签定义用的时候引入即可
     */
    List<UserDto> findUserBySql();
}
