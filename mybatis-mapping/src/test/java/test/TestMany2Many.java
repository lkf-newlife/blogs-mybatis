package test;

import many2many.Role;
import many2many.RoleMapper;
import many2many.User;
import many2many.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestMany2Many {
    private InputStream inputStream = null;
    private SqlSession session = null;
    private UserMapper userMapper = null; // user到role的一对多
    private RoleMapper roleMapper = null; // role到user的一对多

    @Before
    public void init() throws IOException {
        // 1.读取配置文件
        inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        // 2.创建SqlSessionFactory工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        // 3.使用工厂生产SqlSession对象
        session = factory.openSession();
        // 4.使用SqlSession创建Dao接口的代理对象
        userMapper = session.getMapper(UserMapper.class);
        roleMapper = session.getMapper(RoleMapper.class);
        // 5.执行对象方法,即下面的各个测例
    }

    @After
    public void destroy() throws IOException {
        /**
         * 释放资源前提交事务，写在destory()内可以减少代码冗余，免得都写
         *
         * 若是碰到增删改的操作时，千万不要误以为不用提交事务！！！
         */
        session.commit();

        // 6.释放资源
        session.close();
        inputStream.close();
    }

    /**
     * user到role的一对多
     * 需求：查询用户时，可以同时得到用户所包含的角色信息
     */
    @Test
    public void testMany2Many1() {
        List<User> users = userMapper.findAllUser();
        users.forEach(u -> System.out.println(u));
        /**
         * User(id=41, username=张三, birthday=2018-02-27 17:47:08, sex=男, address=北京,
         * roles=[Role(roleId=1, roleName=院长, roleDesc=管理整个学院, users=null),
         * Role(roleId=2, roleName=总裁, roleDesc=管理整个公司, users=null)])
         *
         * User(id=42, username=李四, birthday=2018-03-02 15:09:37, sex=女, address=北京, roles=[])
         * User(id=43, username=王五, birthday=2018-03-04 11:34:34, sex=女, address=北京金燕龙, roles=[])
         *
         * User(id=45, username=赵六, birthday=2018-03-04 12:04:06, sex=男, address=北京金燕龙,
         * roles=[Role(roleId=1, roleName=院长, roleDesc=管理整个学院, users=null)])
         *
         * User(id=46, username=小明, birthday=2018-03-07 17:37:26, sex=男, address=北京, roles=[])
         * User(id=48, username=小红, birthday=2018-03-08 11:44:00, sex=女, address=北京修正, roles=[])
         */
    }

    /**
     * role到user的一对多
     * 需求：查询角色时，可以同时得到角色所赋予的用户信息
     */
    @Test
    public void testMany2Many2() {
        List<Role> roles = roleMapper.findAllRole();
        roles.forEach(role -> System.out.println(role));
        /**
         * Role(roleId=1, roleName=院长, roleDesc=管理整个学院,
         * users=[User(id=41, username=张三, birthday=2018-02-27 17:47:08, sex=男, address=北京, roles=null),
         * User(id=45, username=赵六, birthday=2018-03-04 12:04:06, sex=男, address=北京金燕龙, roles=null)])
         *
         * Role(roleId=2, roleName=总裁, roleDesc=管理整个公司,
         * users=[User(id=41, username=张三, birthday=2018-02-27 17:47:08, sex=男, address=北京, roles=null)])
         *
         * Role(roleId=3, roleName=校长, roleDesc=管理整个学校, users=[])
         */
    }
}
