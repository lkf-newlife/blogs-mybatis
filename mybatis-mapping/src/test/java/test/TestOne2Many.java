package test;

import one2many.User;
import one2many.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestOne2Many {
    private InputStream inputStream = null;
    private SqlSession session = null;
    private UserMapper userMapper = null;

    @Before
    public void init() throws IOException {
        // 1.读取配置文件
        inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        // 2.创建SqlSessionFactory工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        // 3.使用工厂生产SqlSession对象
        session = factory.openSession();
        // 4.使用SqlSession创建Dao接口的代理对象
        userMapper = session.getMapper(UserMapper.class);
        // 5.执行对象方法,即下面的各个测例
    }

    @After
    public void destroy() throws IOException {
        /**
         * 释放资源前提交事务，写在destory()内可以减少代码冗余，免得都写
         *
         * 若是碰到增删改的操作时，千万不要误以为不用提交事务！！！
         */
        session.commit();

        // 6.释放资源
        session.close();
        inputStream.close();
    }

    /**
     * 需求：查询所有用户，同时获取出每个用户下的所有账户信息。
     * 分析：用户信息和他的账户信息为一对多关系，并且查询过程中如果用户没有账户信息，
     * 此时也要将用户信息查询出来，我们想到了左外连接查询比较合适。
     */
    @Test
    public void testOne2Many() {
        List<User> users = userMapper.findAllUser();
        users.forEach(u -> System.out.println(u));
        /**
         * User{id=41, username='张三', birthday='2018-02-27 17:47:08', sex='男', address='北京', accounts=[Account{id=3, uid=41, money=2000.0}, Account{id=1, uid=41, money=1000.0}]}
         * User{id=42, username='李四', birthday='2018-03-02 15:09:37', sex='女', address='北京', accounts=[]}
         * User{id=43, username='王五', birthday='2018-03-04 11:34:34', sex='女', address='北京金燕龙', accounts=[]}
         * User{id=45, username='赵六', birthday='2018-03-04 12:04:06', sex='男', address='北京金燕龙', accounts=[Account{id=2, uid=45, money=1000.0}]}
         * User{id=46, username='小明', birthday='2018-03-07 17:37:26', sex='男', address='北京', accounts=[]}
         * User{id=48, username='小红', birthday='2018-03-08 11:44:00', sex='女', address='北京修正', accounts=[]}
         */
    }
}
