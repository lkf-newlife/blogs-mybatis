package test;

import one2one.Account;
import one2one.AccountMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestOne2One {
    private InputStream inputStream = null;
    private SqlSession session = null;
    private AccountMapper accountMapper = null;

    @Before
    public void init() throws IOException {
        // 1.读取配置文件
        inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        // 2.创建SqlSessionFactory工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        // 3.使用工厂生产SqlSession对象
        session = factory.openSession();
        // 4.使用SqlSession创建Dao接口的代理对象
        accountMapper = session.getMapper(AccountMapper.class);
        // 5.执行对象方法,即下面的各个测例
    }

    @After
    public void destroy() throws IOException {
        /**
         * 释放资源前提交事务，写在destory()内可以减少代码冗余，免得都写
         *
         * 若是碰到增删改的操作时，千万不要误以为不用提交事务！！！
         */
        session.commit();

        // 6.释放资源
        session.close();
        inputStream.close();
    }

    /**
     * 需求：查询所有账户信息，并查询该账户所关联的用户名和地址
     * 注意：因为一个账户信息只能供某个用户使用，所以从查询账户信息出发关联查询用户信息为一对一查询。
     * 如果从用户信息出发查询用户下的账户信息则为一对多查询(见下面一对多样例)，因为一个用户可以有多个账户。
     */
    @Test
    public void testOne2One() {

        List<Account> accountList = accountMapper.findAllAccount();
        accountList.forEach(account -> System.out.println(account));
        /**
         * Account{id=1, uid=41, money=1000.0, user=User{username='张三', address='北京'}}
         * Account{id=2, uid=45, money=1000.0, user=User{username='赵六', address='北京金燕龙'}}
         * Account{id=3, uid=41, money=2000.0, user=User{username='张三', address='北京'}}
         */
    }


}
