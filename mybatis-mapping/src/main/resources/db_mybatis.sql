-- scp db.sql root@192.168.181.160:/tmp
-- 进入mysql客户端执行命令"source /tmp/db.sql;"
create database if not exists db_mybatis character set utf8;
use db_mybatis;

DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
`id` int(11) NOT NULL auto_increment,
`username` varchar(32) NOT NULL COMMENT '用户名称',
`birthday` datetime default NULL COMMENT '生日',
`sex` char(1) default NULL COMMENT '性别',
`address` varchar(256) default NULL COMMENT '地址',
PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `t_user`(`id`,`username`,`birthday`,`sex`,`address`)
values (41,'张三','2018-02-27 17:47:08','男','北京'),
(42,'李四','2018-03-02 15:09:37','女','北京'),
(43,'王五','2018-03-04 11:34:34','女','北京金燕龙'),
(45,'赵六','2018-03-04 12:04:06','男','北京金燕龙'),
(46,'小明','2018-03-07 17:37:26','男','北京'),
(48,'小红','2018-03-08 11:44:00','女','北京修正');

-- t_account 账户表
DROP TABLE IF EXISTS `t_account`;
CREATE TABLE `t_account` (
`ID` int(11) NOT NULL COMMENT '编号',
`UID` int(11) default NULL COMMENT '用户编号',
`MONEY` double default NULL COMMENT '金额',
PRIMARY KEY  (`ID`),
KEY `FK_Reference_8` (`UID`),
CONSTRAINT `FK_Reference_8` FOREIGN KEY (`UID`) REFERENCES `t_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `t_account`(`ID`,`UID`,`MONEY`)
values (1,41,1000),(2,45,1000),(3,41,2000);

-- t_role 角色表
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
`ID` int(11) NOT NULL COMMENT '编号',
`ROLE_NAME` varchar(30) default NULL COMMENT '角色名称',
`ROLE_DESC` varchar(60) default NULL COMMENT '角色描述',
PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `t_role`(`ID`,`ROLE_NAME`,`ROLE_DESC`)
values (1,'院长','管理整个学院'),(2,'总裁','管理整个公司'),(3,'校长','管理整个学校');

-- user_role 中间表(多对多使用)
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
`UID` int(11) NOT NULL COMMENT '用户编号',
`RID` int(11) NOT NULL COMMENT '角色编号',
PRIMARY KEY  (`UID`,`RID`),
KEY `FK_Reference_10` (`RID`),
CONSTRAINT `FK_Reference_10` FOREIGN KEY (`RID`) REFERENCES `t_role` (`ID`),
CONSTRAINT `FK_Reference_9` FOREIGN KEY (`UID`) REFERENCES `t_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `t_user_role`(`UID`,`RID`) values (41,1),(45,1),(41,2);