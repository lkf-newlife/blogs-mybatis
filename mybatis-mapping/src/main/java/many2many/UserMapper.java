package many2many;


import java.util.List;

public interface UserMapper {
    /**
     * user到role的一对多
     * 需求：查询用户时，可以同时得到用户所包含的角色信息
     */
    List<User> findAllUser();
}
