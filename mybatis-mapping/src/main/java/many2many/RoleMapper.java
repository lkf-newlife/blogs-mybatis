package many2many;

import java.util.List;

public interface RoleMapper {
    /**
     * role到user的一对多
     * 需求：查询角色时，可以同时得到角色所赋予的用户信息
     */
    List<Role> findAllRole();
}
