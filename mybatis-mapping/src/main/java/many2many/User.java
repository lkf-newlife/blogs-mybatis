package many2many;

import lombok.Data;

import java.util.List;


@Data
public class User {
    private int id;
    private String username;
    private String birthday;
    private String sex;
    private String address;
    List<Role> roles;
}
