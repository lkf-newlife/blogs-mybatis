package one2one;


import lombok.Getter;
import lombok.Setter;

/**
 * 需求：查询所有账户信息，并查询该账户所关联的用户名和地址
 * 因此Account的toString方法打印所有信息，而User的toString方法只打印username和address
 */
@Getter
@Setter
public class Account {
    private Integer id;
    private Integer uid;
    private Double money;
    private User user;

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", uid=" + uid +
                ", money=" + money +
                ", user=" + user +
                '}';
    }
}
