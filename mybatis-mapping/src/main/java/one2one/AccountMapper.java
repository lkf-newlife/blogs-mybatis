package one2one;

import java.util.List;

public interface AccountMapper {
    List<Account> findAllAccount();
}
