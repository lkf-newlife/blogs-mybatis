package one2many;

import java.util.List;

public interface UserMapper {

    /**
     * 查询所有用户，同时获取出每个用户下的所有账户信息
     * @return
     */
    List<User> findAllUser();
}
