package one2many;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 需求：查询所有用户，同时获取出每个用户下的所有账户信息。
 * 因此User类打印所有信息，Account类打印自己信息(此时不含User信息)
 */
@Getter
@Setter
public class User {
    private int id;
    private String username;
    private String birthday;
    private String sex;
    private String address;
    private List<Account> accounts;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", birthday='" + birthday + '\'' +
                ", sex='" + sex + '\'' +
                ", address='" + address + '\'' +
                ", accounts=" + accounts +
                '}';
    }
}
