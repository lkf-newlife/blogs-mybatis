package cn.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    // 特意加个前缀user于列名不同,主要是用于测试resultMap映射
    private Integer userId;
    private String userName;
    private String userAddress;
    private String userSex;
    private Date userBirthday;
}
