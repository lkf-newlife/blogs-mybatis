package cn.pojo;

import lombok.Data;

//用于测试包装类参数(注意xml中文件参数写法)
@Data
public class FindPojo {
    User user;
}
