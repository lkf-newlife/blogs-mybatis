package cn.mapper;

import cn.pojo.FindPojo;
import cn.pojo.User;

import java.util.List;

/**
 * mapper层即持久层，与以前的dao层一样。
 * 也可以写dao.UserDao,配置文件写成UserDao.xml
 * 但是mybatis提供的是mapper标签，因此用mapper居多，
 * 也可以写成dao层，两者完全一样
 */
public interface UserMapper {
    void addUser(User user);

    void deleteUser(Integer id);

    void updateUser(User user);

    //包装类参数(注意xml中文件参数写法)
    List<User> findByPojo(FindPojo pojo);

    List<User> findAll();
}
