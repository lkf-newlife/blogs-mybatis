package cn.mapper;

import cn.pojo.FindPojo;
import cn.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

public class TestUserMapper {

    /**
     * 注：增删改提交事务的方法都放在 @After public void destory()方法中
     * 别没看全以为不用提交事务！
     */
    private InputStream inputStream;
    private SqlSession sqlSession;
    private UserMapper userMapper;

    @Before
    public void init() throws IOException {
        inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession = factory.openSession();
        userMapper = sqlSession.getMapper(UserMapper.class);
    }

    @After
    public void destory() throws IOException {
        //释放资源前提交事务，写在destory()内可以减少代码冗余，免得都写
        sqlSession.commit();
        //6.释放资源
        sqlSession.close();
        inputStream.close();
    }

    @Test
    public void addUser() {
        User user = new User();
        user.setUserName("张良");
        user.setUserBirthday(new Date());
        user.setUserSex("男");
        user.setUserAddress("郑州");
        userMapper.addUser(user);
    }

    @Test
    public void deleteUser() {
        userMapper.deleteUser(49);
    }

    @Test
    public void updateUser() {
        User user = new User();
        user.setUserName("孙悟空");
        user.setUserBirthday(new Date());
        user.setUserSex("男");
        user.setUserAddress("水帘洞");
        user.setUserId(48);
        userMapper.updateUser(user);
    }

    @Test
    public void findByPojo() {
        FindPojo pojo = new FindPojo();
        User user = new User();
        user.setUserName("%小%");
        pojo.setUser(user);
        List<User> users = userMapper.findByPojo(pojo);
        for (User u : users) {
            System.out.println(u);
        }
    }

    @Test
    public void findAll() {
        List<User> users = userMapper.findAll();
        for (User user : users) {
            System.out.println(user);
        }
    }
}