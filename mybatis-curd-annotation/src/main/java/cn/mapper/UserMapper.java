package cn.mapper;

import cn.pojo.FindPojo;
import cn.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 个人建议：短sql用注解，太长的sql写在xml文件中，否则方法上一大堆太丑
 * 注：定义了@Results(id = "userMap")之后，以后可以用@ResultMap("userMap")进行引用，
 * 例如下面的findByPojo方法可以不写SQL别名，添加注解@ResultMap("userMap")即可
 * 定义@Results的方法(即findAll方法)无需使用，若是加上会编译报错！！！
 */
public interface UserMapper {

    @Insert("INSERT INTO t_user (username, birthday, sex, address)" +
            "VALUES (#{userName}, #{userBirthday}, #{userSex}, #{userAddress})")
    void addUser(User user);

    @Delete("DELETE FROM t_user WHERE id = #{id}")
    void deleteUser(Integer id);

    @Update("UPDATE t_user  SET username = #{userName},  address = #{userAddress}," +
            "sex = #{userSex}, birthday = #{userBirthday}  WHERE id = #{userId}")
    void updateUser(User user);


    @Select("SELECT ID AS userId, USERNAME AS userName, BIRTHDAY AS userBirthday," +
            "SEX AS userSex, ADDRESS AS userAddress FROM t_user WHERE USERNAME LIKE #{user.userName}")
    List<User> findByPojo(FindPojo pojo); // 包装类参数

    @Select("SELECT * FROM t_user")
    @Results(id = "userMap",
            value = {
                    @Result(property = "userId", column = "id"),
                    @Result(property = "userName", column = "username"),
                    @Result(property = "userAddress", column = "address"),
                    @Result(property = "userSex", column = "sex"),
                    @Result(property = "userBirthday", column = "birthday")
            }
    )
    List<User> findAll();
}
